package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@ManagedBean
@SessionScoped
@Named("taskBean")
public class TaskBean {

    private IProjectService projectService;
    private ITaskService taskService;

    private List<Task> taskList;

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    public void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }


    public List<Task> getTaskList(@NotNull final String projectId) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = authentication.getName();
        taskList = taskService.findByUsernameAndProjectId(username,projectId);
        return taskList;
    }

    public String findTaskList(String projectId) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = authentication.getName();
        final List<Task> taskList = taskService.findByUsernameAndProjectId(username,projectId);
        return "/taskListJSF.xhtml";
    }
}
