package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import java.util.List;

@ManagedBean
@SessionScoped
@Named("projectBean")
public class ProjectBean {

    private IUserService userService;

    private IProjectService projectService;

    private List<Project> projectList;

    private String projectId;

    private Project project;


    private String[] selectedReadinessStatus = new String[]{
            ReadinessStatus.PLANNED.displayName(),
            ReadinessStatus.PROCESSING.displayName(),
            ReadinessStatus.READY.displayName()};

    public String[] getSelectedReadinessStatus() {
        return selectedReadinessStatus;
    }

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public List<Project> getProjectList() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = authentication.getName();
        projectList = projectService.findByUsername(username);
        return projectList;
    }

    public Project getProject() {
        return project;
    }

    public String getProjectId() {
        return projectId;
    }

    public Project getOneProject(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = authentication.getName();
        return projectService.findByIdAndUsername(projectId, username);
    }

    public String findTaskList(String projectId) {
        this.projectId = projectId;
        return "taskListJSF.xhtml";
    }

    public List<Project> deleteProject(@Nullable final String projectId) {
        projectService.remove(projectId);
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = authentication.getName();
        projectList = projectService.findByUsername(username);
        return projectList;
    }

    public String editProject(@Nullable final String projectId) {
        this.projectId = projectId;
        return "projectEditJSF.xhtml";
    }

    public String newProject() {
        project = new Project();
        return "newProjectJSF.xhtml";
    }

    public String createProject() {
        final Authentication authentication = SecurityContextHolder.getContext().
                getAuthentication();
        final String username = authentication.getName();
        project.setUser(userService.findByUsername(username));
        project.setReadinessStatus(ReadinessStatus.PLANNED);
        projectService.persist(project);
        projectList = projectService.findByUsername(username);
        return "projectListJSF.xhtml";
    }
}
