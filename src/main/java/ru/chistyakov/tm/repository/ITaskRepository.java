package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chistyakov.tm.entity.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {


    @Nullable
    @Cacheable("findByUserId")
    List<Task> findByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    @Nullable
    List<Task> findByUserIdAndNameLikeOrDescriptionLikeIgnoreCase(@NotNull String userId,@NotNull String name, @Nullable String description);

    @Nullable
    List<Task> findByUserIdOrderByDateBeginTaskAsc(@NotNull String id);

    @Nullable
    List<Task> findByUserIdOrderByDateEndTaskAsc(@NotNull String id);

    @Nullable
    List<Task> findByUserIdOrderByReadinessStatusAsc(@NotNull String id);

    @Nullable
    List<Task> findByProjectId(@NotNull String projectId);

    @Nullable
    List<Task> findByUserUsernameAndProjectId (@NotNull String username,String projectId);

    Task findByUserIdAndId(@NotNull String userId,@NotNull String id);
}
