package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;

import java.util.Collection;

public interface IUserService {

    @Transactional
    boolean updateUser(@Nullable User user);

    @Transactional
    User registryUser(@Nullable User user);

    @Nullable User findById(String userId);

    @Nullable User findByUsername(@Nullable String login);

    void deleteAccount(@Nullable String userId);

    @Nullable Collection<User> findAll();

    @NotNull User mapUserDtoToUser(@NotNull UserDTO userDTO);

    @NotNull UserDTO mapUserToUserDto(@NotNull User user);
}
