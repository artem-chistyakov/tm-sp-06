package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;

import java.util.List;

public interface IProjectService {
    @Transactional
    @Nullable
    Project merge(@Nullable Project project);

    @Transactional
    @Nullable
    Project persist(@Nullable Project project);

    @Transactional
    void removeAll(@Nullable String userId);

    @Transactional
    void remove(@Nullable String id);

    @Nullable Project findOne(@Nullable String id);

    @Nullable List<Project> findByUsername(@Nullable String username);

    @Nullable List<Project> findAllByUserId(@Nullable String userId);

    @Nullable List<Project> findAllInOrderDateBegin(@Nullable String userId);

    @Nullable List<Project> findAllInOrderDateEnd(@Nullable String userId);

    @Nullable List<Project> findAllInOrderReadinessStatus(@Nullable String userId);

    @Nullable List<Project> findByPartNameOrDescription(@Nullable String userId, @Nullable String part);

    @Nullable List<Project> findAll();

    @Nullable Project mapDtoToProject(@NotNull ProjectDTO projectDTO);

    @Nullable ProjectDTO mapProjectToDto(@NotNull Project project);

    @Nullable Project findByIdAndUsername(@Nullable String projectId,@Nullable String username);
}
