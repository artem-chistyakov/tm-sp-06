package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {
    @Transactional
    @Nullable
    Task merge(@Nullable Task task);

    @Transactional
    @Nullable
    Task persist(@Nullable Task task);

    @Nullable Task findOne(@Nullable String id);

    @Nullable Collection<Task> findAllByUserId(@Nullable String userId);


    @Transactional
    void remove(@Nullable Task task);

    @Transactional
    void removeAll(@Nullable String userId);

    @Nullable Collection<Task> findAllInOrderDateBegin(@Nullable String userId);

    @Nullable Collection<Task> findAllInOrderDateEnd(@Nullable String userId);

    @Nullable Collection<Task> findAllInOrderReadinessStatus(@Nullable String userId);

    @Nullable Collection<Task> findByPartNameOrDescription(@Nullable String userId, @Nullable String part);

    @Nullable List<Task> findAll();

    @NotNull Task mapDtoToTask(@NotNull TaskDTO taskDTO);

    @NotNull TaskDTO mapTaskToDto( @NotNull Task task);

    List<Task> findByUsernameAndProjectId(@NotNull  String username, @NotNull  String projectId);

    Task findByUserIdAndTaskId(@NotNull String userId,@NotNull String taskId);
}
