package ru.chistyakov.tm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "tasks")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractEntity {
    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;
    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false, updatable = false)
    private Project project;
    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "readiness_status", nullable = false)
    private ReadinessStatus readinessStatus = ReadinessStatus.PLANNED;
    @Nullable
    private String description;
    @Nullable
    @Column(name = "date_begin")
    private Date dateBeginTask;
    @Nullable
    @Column(name = "date_end")
    private Date dateEndTask;
}

