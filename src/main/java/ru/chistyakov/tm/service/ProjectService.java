package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;

import java.util.List;

@Service
public class ProjectService implements ru.chistyakov.tm.api.IProjectService {

    private IProjectRepository projectRepository;
    private ITaskRepository taskRepository;
    private IUserRepository userRepository;

    @Autowired
    public void setProjectRepository(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Autowired
    public void setTaskRepository(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setUserRepository(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    @Nullable
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    @Nullable
    public Project persist(@Nullable final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public @Nullable Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).get();
    }

    @Override
    public @Nullable List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findByUserId(userId);
    }

    @Override
    public @Nullable List<Project> findByUsername(@Nullable String username) {
        if (username == null) return null;
        return projectRepository.findByUserUsername(username);
    }

    @Override
    public @Nullable List<Project> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findByUserIdOrderByDateBeginProjectAsc(userId);
    }

    @Override
    public @Nullable List<Project> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findByUserIdOrderByDateEndProjectAsc(userId);
    }

    @Override
    public @Nullable List<Project> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findByUserIdOrderByReadinessStatusAsc(userId);
    }

    @Override
    public @Nullable List<Project> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        return projectRepository.findByUserIdAndNameLikeOrDescriptionLike(userId, part, part);

    }

    @Override
    public @Nullable List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @Nullable Project mapDtoToProject(final @NotNull ProjectDTO projectDTO) {
        if (projectDTO.getUserId() == null) return null;
        final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateBeginProject(projectDTO.getDateBeginProject());
        project.setDateEndProject(projectDTO.getDateEndProject());
        project.setUser(userRepository.findById(projectDTO.getUserId()).get());
        project.setTasks(taskRepository.findByProjectId(projectDTO.getId()));
        project.setReadinessStatus(projectDTO.getReadinessStatus());
        return project;
    }

    @Override
    public @Nullable ProjectDTO mapProjectToDto(final @NotNull Project project) {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBeginProject(project.getDateBeginProject());
        projectDTO.setDateEndProject(projectDTO.getDateEndProject());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setReadinessStatus(project.getReadinessStatus());
        return projectDTO;
    }
    public Project findByIdAndUsername(@Nullable String projectId, @Nullable String username){
        if (projectId == null || username == null) return null;
        return projectRepository.findByIdAndUserUsername(projectId,username);
    }
}