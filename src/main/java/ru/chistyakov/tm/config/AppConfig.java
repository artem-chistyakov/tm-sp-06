package ru.chistyakov.tm.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@ComponentScan("ru.chistyakov.tm")
@Import(value = {WebSecurityConfig.class, WebConfig.class, DataConfig.class})
@EnableFeignClients
public class AppConfig {

}
