package ru.chistyakov.tm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("ru.chistyakov.tm.repository")
@PropertySource("classpath:application.properties")
public class DataConfig {

    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.user}")
    private String user;
    @Value("${jdbc.password}")
    private String password;
    @Value("${hibernate.show.sql}")
    private String showSql;
    @Value("${java.boolean.true}")
    private String booleanTrue;
    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernateHbm2ddlAuto;
    @Value("${hibernate.update}")
    private String hibernateUpdate;
    @Value("${hibernate.dialect}")
    private String hibernateDialect;
    @Value("${org.hibernate.dialect.MySQL5Dialect}")
    private String orgHibernateDialectMySQL5Dialect;
    @Value("${hibernate.cache.use_second_level_cache}")
    private String hibernateCacheUseSecondLevelCache;
    @Value("${java.boolean.false}")
    private String javaBooleanFalse;
    @Value("${hibernate.cache.region.factory_class}")
    private String hibernateCacheRegionFactoryClass;
    @Value("${com.hazelcast.hibernate.HazelcastCacheRegionFactory}")
    private String comHazelcastHibernateHazelcastCacheRegionFactory;

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource =
                new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.chistyakov.tm");
        final Properties properties = new Properties();
        properties.put(showSql, booleanTrue);
        properties.put(hibernateHbm2ddlAuto, hibernateUpdate);
        properties.put(hibernateDialect,
                orgHibernateDialectMySQL5Dialect);
        properties.put(hibernateCacheUseSecondLevelCache, javaBooleanFalse);
        properties.put(hibernateCacheRegionFactoryClass, comHazelcastHibernateHazelcastCacheRegionFactory);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(final LocalContainerEntityManagerFactoryBean emf) {
        final JpaTransactionManager transactionManager =
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }
}
