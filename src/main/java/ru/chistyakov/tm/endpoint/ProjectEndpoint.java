package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectEndpoint {
    private IProjectService projectService;

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public List<ProjectDTO> findAllProject() {
        final List<Project> projectList = projectService.findAll();
        final List<ProjectDTO> projectDTOList = new ArrayList<>();
        if (projectList != null)
            for (Project project : projectList) projectDTOList.add(projectService.mapProjectToDto(project));
        return projectDTOList;
    }

    @GetMapping(value = "/{id}")
    public ProjectDTO findProjectById(@NotNull final @PathVariable(name = "id") String projectId) {
        return projectService.mapProjectToDto(projectService.findOne(projectId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProjectDTO createProject(@Nullable final @RequestBody Project resource) {
        if (resource == null) throw new IllegalArgumentException("Ресурс не найден");
        return projectService.mapProjectToDto(projectService.persist(resource));
    }
}
