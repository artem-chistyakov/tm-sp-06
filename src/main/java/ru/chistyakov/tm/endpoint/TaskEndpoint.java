package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskEndpoint {

    private ITaskService taskService;

    @Autowired
    public void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public List<TaskDTO> findAllTask() {
        final List<Task> taskList = taskService.findAll();
        final List<TaskDTO> taskDTOList = new ArrayList<>();
        if (taskList != null)
            for (Task task : taskList) taskDTOList.add(taskService.mapTaskToDto(task));
        return taskDTOList;
    }

    @GetMapping(value = "/{id}")
    public TaskDTO findTaskById(@NotNull final @PathVariable(name = "id") String taskId) {
        return taskService.mapTaskToDto(taskService.findOne(taskId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDTO createTask(@Nullable final @RequestBody TaskDTO taskDTO) {
        return taskService.mapTaskToDto(taskService.persist(taskService.mapDtoToTask(taskDTO)));
    }
}
