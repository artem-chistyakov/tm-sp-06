package ru.chistyakov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.TaskDTO;

import java.util.List;

@FeignClient(url = "http://localhost:8080/task-manager")
public interface SimpleClient {

    @GetMapping("/projects")
    List<ProjectDTO> findAllProject();

    @GetMapping("/tasks")
    List<TaskDTO> findAllTask();

    @GetMapping( "/projects/{id}")
    ProjectDTO  findProjectById(@PathVariable("id") String projectId);

    @GetMapping( "/tasks/{id}")
    TaskDTO  findTaskById(@PathVariable("id") String taskId);

}
